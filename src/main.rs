use clap::Parser;
use std::thread::sleep;
use std::time;

/// Simple CLI Pomodoro timer
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// How long we are working for in minutes
    #[arg(short)]
    work_time: u8,
    /// How long we are going to rest for in minutes
    #[arg(short)]
    rest_time: u8,
    /// How many iteration before we are stopping for a long break.
    #[arg(short)]
    iterations: u8,
}

fn convert_to_seconds(minutes: &u8) -> u16 {
    // Converts minutes to seconds, Simple really
    // Need to convert to u16 for a cleaner expression below. 
    
    let seconds: u16 = <u8 as Into<u16>>::into(*minutes) * 60;

    seconds.into()
}

fn countdown(seconds: u16) {
    let mut seconds_remaining: u16 = seconds;

    while seconds_remaining > 0 {
        println!("{}", seconds_remaining);
        seconds_remaining -= 1;
        sleep(time::Duration::from_secs(1))
    }

}

fn main() {
    let args = Args::parse();

    let work_time_as_seconds = convert_to_seconds(&args.work_time);
    let rest_time_as_seconds = convert_to_seconds(&args.rest_time);

    for i in 1..=args.iterations {

        println!("Round number: {}", i);
        println!("Time to focus on work, keep it up for {:?} minutes", args.work_time);
        countdown(work_time_as_seconds);

        println!("Let us take a {} minute break", args.rest_time);
        countdown(rest_time_as_seconds);

        let runsleft: u8 = args.iterations - i;

        if runsleft > 1 {
            println!("We are going to do this {} times before taking a long break.", runsleft);
        } else if runsleft == 1 {
            println!("Last round, lets go!");
        } else {
            println!("We are done!. Have a well deserved break");
        }

    };


}
